#!/bin/bash
# This script is designed for local usage.

# Ignore generated files
# export IGNORE_LINT="platform_venv|"

# Ignore generated related files
export IGNORE_LINT='platform_venv|migrations|nest'
export VIRTUAL_ENV=$NRP_VIRTUAL_ENV

# This script only runs static code analysis, the tests can be run separately using run_tests.sh
make run_pycodestyle run_pylint
RET=$?

if [ $RET == 0 ]; then
    echo -e "\033[32mVerify sucessfull.\e[0m Run ./run_tests.sh to run the tests."
else
    echo -e "\033[31mVerify failed.\e[0m"
fi

exit $RET
