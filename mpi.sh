#!/usr/bin/env bash

logdir=$1
exec 1> $logdir/srun.log 2>&1
shift

echo "[mpi.sh] Running mpi.sh script. Logs in: $logdir"
cat /home2/bbpnrsoa/nrp/src/BrainSimulation/mpi.sh

echo -n "[mpi.sh] CWD: "
pwd

# Set CLE environment
export LC_ALL=C
unset LANGUAGE
. /home2/bbpnrsoa/nrp/src/user-scripts/nrp_variables

echo $PYTHONPATH

export ROS_MASTER_URI=http://$1:11311
echo "[mpi.sh] ROS_MASTER_URI: $ROS_MASTER_URI"
shift

export ROS_IP=$(hostname -I | cut -d " " -f 1)
echo "[mpi.sh] ROS_IP: $ROS_IP"

export GAZEBO_MASTER_URI=http://$(hostname -I | cut -d " " -f 1):11345
echo "[mpi.sh] GAZEBO_MASTER_URI : $GAZEBO_MASTER_URI"

rm /usr/lib/libmpi.so.12
rm /usr/lib/libmpi.so.12.0.2
ln -s /usr/lib/libmpi.so.12.0.5 /usr/lib/libmpi.so.12
export LD_LIBRARY_PATH=/usr/lib:$LD_LIBRARY_PATH
echo "[mpi.sh] Executing the MPI command: $@"
$@ >> $logdir/mpi.log 2>&1