import sys, os
# import conf.py from admin-scripts
sys.path.insert(0, os.environ.get('HBP') + '/admin-scripts/ContinuousIntegration/python/docs')
from sphinxconf import * # pylint: disable=import-error

# import modules needed by this project
sys.path.insert(0, os.path.abspath('../../hbp_nrp_distributed_nest'))  # Source code dir relative to this file
import hbp_nrp_distributed_nest

# -- General configuration -----------------------------------------------------
# Add any Sphinx extension module names here, as strings. 
# The common extensions are defined in admin-scripts
# extensions = []

apidoc_module_dir = '../../hbp_nrp_distributed_nest'

# General information about the project.
project = u'Brain Simulation'

# Output file base name for HTML help builder.
htmlhelp_basename = 'hbp_nrp_distributed_nest_doc'

# -- Options for manual page output --------------------------------------------

# One entry per manual page. List of tuples
# (source start file, name, description, authors, manual section).
man_pages = [
    ('index', 'hbp_nrp_distributed_nest', u'Brain Simulation Documentation',
     [u'Sharma', u'Akl'], 1)
]

# -- Options for Texinfo output ------------------------------------------------

# Grouping the document tree into Texinfo files. List of tuples
# (source start file, target name, title, author,
#  dir menu entry, description, category)
texinfo_documents = [
  ('index', 'hbp_nrp_distributed_nest', u'Brain Simulation Documentation',
   u'Human Brain Project', 'hbp_nrp_distributed_nest', '',
   'Miscellaneous'),
]

# -- Mocking for importing external modules ------------------------------------

# the following modules are part of CLE and should be mocked in the ExDBackend
autodoc_mock_imports = ['pyNN.nest', 'nest']


