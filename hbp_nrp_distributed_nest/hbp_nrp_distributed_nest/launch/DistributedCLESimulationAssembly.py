# https://www.humanbrainproject.eu
#
# The Human Brain Project is a European Commission funded project
# in the frame of the Horizon2020 FET Flagship plan.
# http://ec.europa.eu/programmes/horizon2020/en/h2020-section/fet-flagships
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
# ---LICENSE-END
"""
This module contains the distributed Nest process logic and simulation assembly
"""

import logging

from hbp_nrp_cleserver.server.CLEGazeboSimulationAssembly import CLEGazeboSimulationAssembly
from hbp_nrp_cleserver.server.ServerConfigurations import robot_gazebo_ros_adapters
from hbp_nrp_distributed_nest.cle.DistributedPyNNCommunicationAdapter \
       import DistributedPyNNCommunicationAdapter
from hbp_nrp_distributed_nest.cle.DistributedPyNNControlAdapter \
       import DistributedPyNNControlAdapter

import pyNN.nest as sim

__author__ = 'Hossain Mahmud'
logger = logging.getLogger(__name__)


class DistributedCLESimulationAssembly(CLEGazeboSimulationAssembly):
    """
    Defines the assembly of a distributed Nest simulation
    """

    def __init__(self, sim_config):
        """
        Creates a new simulation assembly to simulate an experiment using the CLE and Gazebo

        :param sim_config: A sim config object that "similar" to the one used for the CLE process
        """
        super(DistributedCLESimulationAssembly, self).__init__(sim_config)

    def _create_robot_adapters(self):
        """
        Creates the adapter components for the robot side

        :return: A tuple of the communication and control adapter for the robot side
        """
        return robot_gazebo_ros_adapters()

    def _create_brain_adapters(self):
        """
        Creates the adapter components for the neural simulator

        :return: A tuple of the communication and control adapter for the neural simulator
        """
        logger.info('Using distributed configuration and adapters for CLE')

        # return the assembled control adapters
        braincomm = DistributedPyNNCommunicationAdapter()
        braincontrol = DistributedPyNNControlAdapter(sim)
        return braincomm, braincontrol

