# ---LICENSE-BEGIN - DO NOT CHANGE OR MOVE THIS HEADER
# This file is part of the Neurorobotics Platform software
# Copyright (C) 2014,2015,2016,2017 Human Brain Project
# https://www.humanbrainproject.eu
#
# The Human Brain Project is a European Commission funded project
# in the frame of the Horizon2020 FET Flagship plan.
# http://ec.europa.eu/programmes/horizon2020/en/h2020-section/fet-flagships
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
# ---LICENSE-END
"""
localhost launch target configuration.
"""

from hbp_nrp_distributed_nest.launch.host import IHostLauncher

import os
import shutil
import tempfile


class LocalLauncher(IHostLauncher):
    """
    This launch configuration targets the localhost for all processes and is suitable for local
    installs or deployed installs where the newly spawned processes can run on the same host
    as the REST backend.
    """

    def __init__(self):
        """
        Create a local launcher to target localhost.
        """
        super(LocalLauncher, self).__init__()

        # this launcher only targets the localhost
        self._hostname = 'localhost'

        # create a temporary directory for configuration files (the same for local/host)
        self._local_tmpdir = tempfile.mkdtemp()
        self._host_tmpdir = self._local_tmpdir

    def create_launch_scripts(self):
        """
        Nothing to create since the target is localhost.
        """
        pass

    def deploy(self):
        """
        Nothing to deploy since the target is localhost.
        """
        pass

    def shutdown(self):
        """
        Shutdown by trying to kill any running processes and deleting the temporary directory.
        """

        if self._local_tmpdir is not None and os.path.exists(self._local_tmpdir):

            # finally, delete the directory
            shutil.rmtree(self._local_tmpdir)

        # even though this class should not be reused, unset the tmpdir
        self._local_tmpdir = None
        self._host_tmpdir = None
