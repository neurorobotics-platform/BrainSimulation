"""
This package contains host specific implementations for different distributed
simulation targets.
"""


from builtins import object
class IHostLauncher(object):
    """
    A generic interface to implement a host specific launcher. Guarantees necessary property and
    functions are accessible in inherited classes.
    """

    def __init__(self):
        """
        Interface constructor, guarantee that the hostname and tmpdir properties are available.
        """
        self._hostname = None
        self._local_tmpdir = None
        self._host_tmpdir = None

    @property
    def hostname(self):
        """
        Return the target host for the launcher implementation. Raise an exception if the host
        specific implementation does not set the hostname value.
        """
        if not self._hostname:
            raise NotImplementedError('Host specific implementation did not set target hostname!')
        return self._hostname

    @property
    def local_tmpdir(self):
        """
        Return the temporary configuration directory that can be used to write
        configuration files. Raise an exception if the host specific implementation does not set
        the tmpdir value.
        """
        if not self._local_tmpdir:
            raise NotImplementedError('Host specific implementation did not set temp directory!')
        return self._local_tmpdir

    @property
    def host_tmpdir(self):
        """
        Return the temporary execution directory on the host that contains all necessary
        configuration files. Raise an exception if the host specific implementation does not set
        the tmpdir value.
        """
        if not self._host_tmpdir:
            raise NotImplementedError('Host specific implementation did not set temp directory!')
        return self._host_tmpdir

    def create_launch_scripts(self):
        """
        Create a set of launch scripts for the CLE and individual brain processes with specific
        implementations required for each host. Write the launch scripts to the temporary directory
        for this launcher.

        Returns a tuple (path to CLE launch script, path to BrainProcess launc script).
        """
        raise NotImplementedError('Host specific implementation is missing!')

    def deploy(self):
        """
        Deploy the temporary directory contents to the target host if necessary.
        """
        raise NotImplementedError('Host specific implementation is missing!')

    def shutdown(self):
        """
        Shutdown and cleanup any host specific configuration.
        """
        raise NotImplementedError('Host specific implementation is missing!')
