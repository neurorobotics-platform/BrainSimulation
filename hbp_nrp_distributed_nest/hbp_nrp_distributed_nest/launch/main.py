# https://www.humanbrainproject.eu
#
# The Human Brain Project is a European Commission funded project
# in the frame of the Horizon2020 FET Flagship plan.
# http://ec.europa.eu/programmes/horizon2020/en/h2020-section/fet-flagships
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
# ---LICENSE-END
"""
Entry point of distributed CLE and NEST
"""
from __future__ import print_function
import sys
import signal

# import pyNN.nest here to ensure NEST ranks are initialized correctly
import pyNN.nest as sim
import nest
nest.ll_api.set_debug(False)

import hbp_nrp_distributed_nest.launch.DistributedCLEProcess as DistCLE
from hbp_nrp_distributed_nest.launch.DistributedNestProcess import launch_brain

argv_backup = list(sys.argv[1:])
sys.argv = [sys.argv[0]]


def handle_sigterm(signo, stack_frame):
    message_template = '[ MPI ] ================ {message} ================ {rank}'
    print(message_template.format(message="received sigterm", rank=str(rank)))
    if DistCLE.simulation is not None:
        print(message_template.format(message="shutdown on sigterm", rank=str(rank)))
    DistCLE.simulation.shutdown()
    sys.exit(0)


if __name__ == '__main__':  # pragma: no cover

    from mpi4py import MPI
    rank = MPI.COMM_WORLD.Get_rank()
    import socket
    hostname = socket.gethostname()

    print('[ MPI ] ========== nest rank={}; hostname={} ========'.format(nest.Rank(), hostname))

    # use the MPI process rank to determine if we should launch CLE or brain process
    # both launch commands are blocking until shutdown occurs
    signal.signal(signal.SIGTERM, handle_sigterm)

    print('[ MPI ] ========== initialized={} with thread_level={} ========'.format(
        str(MPI.Is_initialized()), str(MPI.Query_thread())))
    if not MPI.Is_initialized():
        MPI.Init_thread(MPI.THREAD_MULTIPLE)

    if rank == 0:
        # import pydevd
        # pydevd.settrace('localhost', port=50003, stdoutToServer=True, stderrToServer=True, suspend=False)
        print('[ MPI ] ================ LAUNCHING CLE  ================ ' + str(rank))
        DistCLE.launch_cle(argv_backup)
    else:
        # import pydevd
        # pydevd.settrace('localhost', port=50004, stdoutToServer=True, stderrToServer=True, suspend=False)
        print('[ MPI ] ================ LAUNCHING NEST ================ ' + str(rank))
        launch_brain(argv_backup)

    print('[ MPI ] Gracefully exit process ' + str(rank))
    sys.exit(0)
