# https://www.humanbrainproject.eu
#
# The Human Brain Project is a European Commission funded project
# in the frame of the Horizon2020 FET Flagship plan.
# http://ec.europa.eu/programmes/horizon2020/en/h2020-section/fet-flagships
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
# ---LICENSE-END
"""
This module contains the distributed Nest process logic and simulation assembly
"""
from __future__ import print_function

# The Nest imports below somehow delete/inject command line arguments that cause
# issues with argparse in each of the launchers, save the valid arguments now and
# clear the argument list...
import sys
#import hbp_nrp_cle.brainsim.pynn_nest # pylint: disable=unused-import

import logging
import argparse
import traceback

from hbp_nrp_distributed_nest.launch.NestBrainProcess import NestBrainProcess

__author__ = 'Hossain Mahmud'
logger = logging.getLogger(__name__)


def launch_brain(argv): # pragma: no cover
    """
    Load the brain process for communication with the master CLE process, run the simulation
    blocking until the MPI processes are terminated by CLE shutdown.
    """

    # import MPI here, must be done after Nest in subclass adapters
    from hbp_nrp_cle.brainsim import COMM_NRP

    try:
        parser = argparse.ArgumentParser()
        parser.add_argument('--exdconf', dest='exd_file',
                            help='specify the ExDConfiguration file', required=True)
        parser.add_argument('--rng-seed', dest='rng_seed',
                            help='the global experiment RNG seed', required=True)
        parser.add_argument('--sim-id', dest='sim_id', type=int,
                            help='the simulation id to use', required=True)
        args, _ = parser.parse_known_args(argv)

        from hbp_nrp_commons.sim_config.SimConfig import SimConfig
        sim_config = SimConfig(args.exd_file,
                               sim_id=args.sim_id,
                               rng_seed=int(args.rng_seed))

        # construct brain and proxies (expand environment variables in paths)
        brain = NestBrainProcess(sim_config)

        # run the brain until terminated, this is a blocking call
        brain.run()

    except Exception as ex: # pylint: disable=broad-except
        # print the traceback which should go back to the remote logger
        traceback.print_exc()
        print(str(ex))
        # for any failures, terminate all other brain processes and the CLE
        print('[ MPI ] ABORTing distributed NEST process: {}'.format(str(COMM_NRP.Get_rank())))
        COMM_NRP.Abort(-1)
