"""
This package contains NRP specific implementations for multi-process, distributed Nest.
"""

from hbp_nrp_distributed_nest.version import VERSION as __version__  # pylint: disable=W0611
