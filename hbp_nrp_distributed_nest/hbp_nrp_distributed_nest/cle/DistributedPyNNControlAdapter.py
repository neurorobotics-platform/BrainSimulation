# https://www.humanbrainproject.eu
#
# The Human Brain Project is a European Commission funded project
# in the frame of the Horizon2020 FET Flagship plan.
# http://ec.europa.eu/programmes/horizon2020/en/h2020-section/fet-flagships
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
# ---LICENSE-END
"""
This module defined a CLE control adapter that notifies all remote brain processes
when they should step the simulation.
"""

from builtins import range
from hbp_nrp_cle.brainsim.pynn.PyNNControlAdapter import PyNNControlAdapter
from hbp_nrp_distributed_nest.launch.NestBrainProcess import NestBrainProcess

from hbp_nrp_cle.brainsim import COMM_NRP


class DistributedPyNNControlAdapter(PyNNControlAdapter):
    """
    This class is required as multi-threading the brain process to receive MPI messages
    while blocking to step the brain causes segmentation faults in the CLE. The overhead
    is minimal here and allows us to have dynamic behavior in the brain processes between
    simulation steps.
    """

    def load_brain(self, network_file, populations):
        """
        Notify all remote brain processes to load the brain with population definitions
        specified.

        TODO: the network_file will have to be accessed from common storage between
              processes for any configuration other than local installataions

        :param network_file: The path to the python file containing the network
        :param populations: A named list of populations to create
        """

        # notify all other processes, blocking send calls for them to receive
        for rank in range(COMM_NRP.Get_size()):
            if rank == COMM_NRP.Get_rank():
                continue
            COMM_NRP.send({'command': 'LoadBrain', 'file': network_file,
                                 'populations': populations},
                                dest=rank, tag=NestBrainProcess.MPI_MSG_TAG)

        # run the actual brain load on this process
        super(DistributedPyNNControlAdapter, self).load_brain(network_file)
        super(DistributedPyNNControlAdapter, self).load_populations(populations)

    def run_step(self, dt):
        """
        Notify all remote brain processes to run a simulation step, then run the step in
        this process.
        """

        # notify all other processes, blocking send calls for them to receive
        for rank in range(COMM_NRP.Get_size()):
            if rank == COMM_NRP.Get_rank():
                continue
            COMM_NRP.send('step', dest=rank, tag=NestBrainProcess.MPI_MSG_TAG)

        # run the actual simulation step
        super(DistributedPyNNControlAdapter, self).run_step(dt)
