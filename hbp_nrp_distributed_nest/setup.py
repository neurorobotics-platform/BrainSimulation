'''setup.py'''

from setuptools import setup

import hbp_nrp_distributed_nest

reqs_file = './requirements.txt'

# Get the list of requirements
install_reqs = list(val.strip() for val in open(reqs_file))
reqs = install_reqs

config = {
    'description': 'Distributed Nest interface support for CLE/ExDBackend for HBP SP10',
    'author': 'HBP Neurorobotics',
    'url': 'http://neurorobotics.net',
    'author_email': 'neurorobotics@humanbrainproject.eu',
    'version': hbp_nrp_distributed_nest.__version__,
    'install_requires': reqs,
    'packages': ['hbp_nrp_distributed_nest',
                 'hbp_nrp_distributed_nest.cle',
                 'hbp_nrp_distributed_nest.launch',
                 'hbp_nrp_distributed_nest.launch.host'],
    'classifiers': ['Programming Language :: Python :: 3'],
    'scripts': [],
    'name': 'hbp-nrp-distributed-nest',
    'include_package_data': True,
}

setup(**config)
